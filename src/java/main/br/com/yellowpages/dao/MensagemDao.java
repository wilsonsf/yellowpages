package br.com.yellowpages.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.yellowpages.jdbc.ConnectionFactory;
import br.com.yellowpages.modelo.Mensagem;
import br.com.yellowpages.modelo.Usuario;

import com.mysql.jdbc.PreparedStatement;


public class MensagemDao extends GenericDao {	

	public MensagemDao () {
		this.conexao = new ConnectionFactory().getConnection();
	}

	public void solicita(Usuario remetente, Usuario destinatario){

		// Associa��o da msg para o usuario remetente
		String sql1 = "INSERT INTO usuarios " +
				"(cpf,nome,email,telefone)" +
				"values(?,?,?,?)";

		// Associa��o da msg para o usuario destinatario
		String sql2 = "INSERT INTO usuarios " +
				"(cpf,nome,email,telefone)" +
				"values(?,?,?,?)";

		//TODO: Associar mensagem padr�o de solicita��o de Rementente para Destinatario 

//		String mensagemPadrao = "remetente lhe enviou uma solicitacao de negocios";

		try {

			// Usuario remetente
			PreparedStatement stmt = (PreparedStatement) this.conexao.prepareStatement(sql1);			

//			stmt.setString(1, usuario.getCpf());
//			stmt.setString(1, usuario.getNome());
//			stmt.setString(2, usuario.getEmail());
//			stmt.setString(3, usuario.getTelefone());

			stmt.execute();
			stmt.close();

			// Usuario destinatario
			stmt = (PreparedStatement) this.conexao.prepareStatement(sql2);

//			stmt.setString(1, usuario.getCpf());
//			stmt.setString(1, usuario.getNome());
//			stmt.setString(2, usuario.getEmail());
//			stmt.setString(3, usuario.getTelefone());

			stmt.execute();
			stmt.close();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	public void adiciona(Usuario remetente, Usuario destinatario, String mensagemEspecifica){

		// Associa��o da msg para o usuario remetente
		String sql1 = "INSERT INTO usuarios " +
				"(cpf,nome,email,telefone)" +
				"values(?,?,?,?)";

		// Associa��o da msg para o usuario destinatario
		String sql2 = "INSERT INTO usuarios " +
				"(cpf,nome,email,telefone)" +
				"values(?,?,?,?)";

		//TODO: Associar mensagem padr�o de solicita��o de Rementente para Destinatario 

		mensagemEspecifica = "alguma mensagem de remetente para destinatario";

		try {

			// Usuario remetente
			PreparedStatement stmt = (PreparedStatement) this.conexao.prepareStatement(sql1);			

//			stmt.setString(1, usuario.getCpf());
//			stmt.setString(1, usuario.getNome());
//			stmt.setString(2, usuario.getEmail());
//			stmt.setString(3, usuario.getTelefone());

			stmt.execute();
			stmt.close();

			// Usuario destinatario
			stmt = (PreparedStatement) this.conexao.prepareStatement(sql2);

//			stmt.setString(1, usuario.getCpf());
//			stmt.setString(1, usuario.getNome());
//			stmt.setString(2, usuario.getEmail());
//			stmt.setString(3, usuario.getTelefone());

			stmt.execute();
			stmt.close();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	public List<Mensagem> getListaMensagem(Usuario usuario){

		try {
			List<Mensagem> mensagens = new ArrayList<Mensagem>();
			PreparedStatement stmt = (PreparedStatement) this.conexao
					.prepareStatement("SELECT * FROM usuario");
			//TODO: Fazendo consulta em Usuario
			//FIXME: Fazer consulta de mensagens de um determinado Usuario

			ResultSet rs = stmt.executeQuery();

//			mensagens = trataResultados(rs);

			rs.close();
			stmt.close();

			return mensagens;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}


	// Metodo para retornar mensagens com conteudo de nome de algum Usuario ? 
	/*public List<Mensagem> pesquisarPorUsuario(String usuario){


	}*/

	// Metodo para retornar mensagens com determinado conteudo ou nome de algum Usuario
	public List<Mensagem> pesquisarPorConteudo(String conteudo){

		try {
			List<Mensagem> mensagens = new ArrayList<Mensagem>();
			PreparedStatement stmt = (PreparedStatement) this.conexao
					.prepareStatement("SELECT * FROM usuario");
			//TODO: Fazendo consulta em Usuario
			//FIXME: Fazer consulta de mensagens de uma determinada mensagem ou palavra

			ResultSet rs = stmt.executeQuery();

//			mensagens = trataResultados(rs);

			rs.close();
			stmt.close();

			return mensagens;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

}
