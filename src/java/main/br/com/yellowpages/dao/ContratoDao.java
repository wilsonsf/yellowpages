package br.com.yellowpages.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.yellowpages.jdbc.ConnectionFactory;
import br.com.yellowpages.modelo.Contrato;
import br.com.yellowpages.modelo.Usuario;

import com.mysql.jdbc.PreparedStatement;

public class ContratoDao extends GenericDao {

	public ContratoDao () {
		this.conexao = new ConnectionFactory().getConnection();
	}

	public void adiciona(Usuario cliente, Usuario prestador, Calendar data){

		// Adicionando Cliente a contrato
		String sql1 = "INSERT INTO usuarios " +
				"(cpf,nome,email,telefone)" +
				"values(?,?,?,?)";

		// Adicionando Prestador a contrato
		String sql2 = "INSERT INTO usuarios " +
				"(cpf,nome,email,telefone)" +
				"values(?,?,?,?)";

		//TODO: Adicionar Contrato com Cliente, Prestador e data incial

		// Exemplo de data de inicio de contrato
		data = Calendar.getInstance();
//		data = "15/11/2015";

		try {

			// Usuario cliente
			PreparedStatement stmt = (PreparedStatement) this.conexao.prepareStatement(sql1);			

//			stmt.setString(1, usuario.getCpf());
//			stmt.setString(1, usuario.getNome());
//			stmt.setString(2, usuario.getEmail());
//			stmt.setString(3, usuario.getTelefone());

			stmt.execute();
			stmt.close();

			// Usuario prestador
			stmt = (PreparedStatement) this.conexao.prepareStatement(sql2);

//			stmt.setString(1, usuario.getCpf());
//			stmt.setString(1, usuario.getNome());
//			stmt.setString(2, usuario.getEmail());
//			stmt.setString(3, usuario.getTelefone());

			stmt.execute();
			stmt.close();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	public void atualiza(){


	}

	public List<Contrato> getListaContrato(Contrato contrato){

		try {
			List<Contrato> contratos = new ArrayList<Contrato>();
			PreparedStatement stmt = (PreparedStatement) this.conexao
					.prepareStatement("SELECT * FROM usuario");
			//TODO: Fazendo consulta em Usuario
			//FIXME: Fazer consulta de contratos de um determinado Usuario

			ResultSet rs = stmt.executeQuery();

//			contratos = trataResultados(rs);

			rs.close();
			stmt.close();

			return contratos;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}
	public void pesquisarData(){


	}


}
