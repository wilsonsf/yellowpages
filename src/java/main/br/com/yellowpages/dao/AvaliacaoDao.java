package br.com.yellowpages.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.yellowpages.modelo.Avaliacao;
import br.com.yellowpages.modelo.Usuario;

import com.mysql.jdbc.PreparedStatement;


public class AvaliacaoDao extends GenericDao {	

	public AvaliacaoDao () {
//		this.conexao = new ConnectionFactory().getConnection();
	}

	public void adiciona(Usuario avaliador, Usuario sujeito, int nota, String comentario) {

		// Adicionando Avaliador
		String sql = "INSERT INTO usuarios " +
				"(cpf,nome,email,telefone)" +
				"values(?,?,?,?)";

		// Adicionando Sujeito
//		String sq2 = "INSERT INTO usuarios " +
//				"(cpf,nome,email,telefone)" +
//				"values(?,?,?,?)";

		// Exemplo de nota
		nota = 5;
		
		// Exemplo de comentario
		comentario = "Fez um servi�o meio b�snia";

		//TODO: Adicionar avaliacao com nota e comentario no usuario avaliado 

		try {
			PreparedStatement stmt = (PreparedStatement) this.conexao.prepareStatement(sql);

//			stmt.setString(1, usuario.getCpf());
//			stmt.setString(1, usuario.getNome());
//			stmt.setString(2, usuario.getEmail());
//			stmt.setString(3, usuario.getTelefone());

			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public List<Avaliacao> getListaAvaliacao(Usuario usuario){

		try {
			List<Avaliacao> avaliacoes = new ArrayList<Avaliacao>();
			PreparedStatement stmt = (PreparedStatement) this.conexao
					.prepareStatement("SELECT * FROM Avaliacoes");
			//TODO: Fazendo consulta em Usuario
			//FIXME: Fazer consulta de avaliacoes de um determinado Usuario

			ResultSet rs = stmt.executeQuery();

			avaliacoes = trataResultados(rs);

			rs.close();
			stmt.close();

			return avaliacoes;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	public List<Avaliacao> trataResultados(ResultSet rs) throws SQLException {

		List<Avaliacao> avaliacoes = new ArrayList<Avaliacao>();

		while (rs.next()) {
			Avaliacao avaliacao = new Avaliacao();
//			avaliacao.setCpf(rs.getString("cpf"));
//			avaliacao.setNome(rs.getString("nome"));
//			avaliacao.setEmail(rs.getString("email"));
//			avaliacao.setTelefone(rs.getString("telefone"));

			//TODO: Modificar a consulta para trazer as categorias 
			//TODO: Fazer o tratamento em List/ArrayList

			avaliacoes.add(avaliacao);
		}

		return avaliacoes;
	}
}