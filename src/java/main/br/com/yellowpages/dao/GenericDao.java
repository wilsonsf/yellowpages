package br.com.yellowpages.dao;

import java.sql.Connection;

import br.com.yellowpages.jdbc.ConnectionFactory;

public class GenericDao {
	protected Connection conexao;
	
	GenericDao() {
		this.conexao = new ConnectionFactory().getConnection();
	}

	// TODO: Implementar caso genérico de consulta dos DAOs
//	public <T> List<T> trataResultados(ResultSet rs) throws SQLException {
//
//		List<T> usuarios = new ArrayList<T>();
//
//		while (rs.next()) {
//			Usuario usuario = new Usuario();
//			usuario.setCpf(rs.getString("cpf"));
//			usuario.setNome(rs.getString("nome"));
//			usuario.setEmail(rs.getString("email"));
//			usuario.setTelefone(rs.getString("telefone"));
//
//			//TODO: Modificar a consulta para trazer as categorias 
//			//TODO: Fazer o tratamento em List/ArrayList
//
//			usuarios.add(usuario);
//		}
//
//		return usuarios;
//	}
}
