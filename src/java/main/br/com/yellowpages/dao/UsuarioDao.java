package br.com.yellowpages.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.yellowpages.modelo.Usuario;

import com.mysql.jdbc.PreparedStatement;

public class UsuarioDao extends GenericDao{

//	public UsuarioDao () {
//		this.conexao = new ConnectionFactory().getConnection();
//	}

	public void adiciona (Usuario usuario) {
		String sql = "INSERT INTO usuarios " +
				"(cpf,nome,email,telefone)" +
				"values(?,?,?,?)";

		try {
			PreparedStatement stmt = (PreparedStatement) this.conexao.prepareStatement(sql);

			stmt.setString(1, usuario.getCpf());
			stmt.setString(1, usuario.getNome());
			stmt.setString(2, usuario.getEmail());
			stmt.setString(3, usuario.getTelefone());

			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void remove (Usuario usuario) {

	}

	public void altera (Usuario usuario) {

	}

	public Usuario pesquisar(String cpf) {
		return new Usuario();
	}

	public List<Usuario> getLista() {
		try {
			List<Usuario> usuarios = new ArrayList<Usuario>();
			PreparedStatement stmt = (PreparedStatement) this.conexao
					.prepareStatement("SELECT * FROM usuarios");
			ResultSet rs = stmt.executeQuery();

			usuarios = trataResultados(rs);

			rs.close();
			stmt.close();

			return usuarios;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public List<Usuario> pesquisarPorNome(String n) {
		try {
			List<Usuario> usuarios = new ArrayList<Usuario>();
			PreparedStatement stmt = (PreparedStatement) this.conexao
					.prepareStatement("SELECT * FROM usuarios WHERE nome like ?");

			//Protege de um problema chamado SQL Injection
			stmt.setString(1, "%" + n + "%"); 

			ResultSet rs = stmt.executeQuery();

			usuarios = trataResultados(rs);

			rs.close();
			stmt.close();

			return usuarios;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public List<Usuario> trataResultados(ResultSet rs) throws SQLException {

		List<Usuario> usuarios = new ArrayList<Usuario>();

		while (rs.next()) {
			Usuario usuario = new Usuario();
			usuario.setCpf(rs.getString("cpf"));
			usuario.setNome(rs.getString("nome"));
			usuario.setEmail(rs.getString("email"));
			usuario.setTelefone(rs.getString("telefone"));

			//TODO: Modificar a consulta para trazer as categorias 
			//TODO: Fazer o tratamento em List/ArrayList

			usuarios.add(usuario);
		}

		return usuarios;
	}
}
