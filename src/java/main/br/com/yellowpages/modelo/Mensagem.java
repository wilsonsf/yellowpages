package br.com.yellowpages.modelo;

import java.util.Calendar;

public class Mensagem {

	// Campos
	
	private Calendar dataDeEnvio;
	private String conteudo;
	private int tipo;
	private boolean ativo;
	private String cpfRemetente;
	private String cpfDestinatario;
	
	
	/**
	 * Construtor para instanciar objeto Mensagem 
	 * @param builder
	 */
	public Mensagem () {
		
	}
	
	public Mensagem(Calendar dataDeEnvio, String conteudo, int tipo, boolean ativo, String cpfRemetente, String cpfDestinatario) {
		this.dataDeEnvio = dataDeEnvio;
		this.conteudo = conteudo;
		this.tipo = tipo;
		this.ativo = ativo;
		this.cpfRemetente = cpfRemetente;
		this.cpfDestinatario = cpfDestinatario;
	}

	// Metodos gets e sets
	
	public Calendar getDataDeEnvio() {
		return dataDeEnvio;
	}
	
	public void setDataDeEnvio(Calendar dataDeEnvio) {
		this.dataDeEnvio = dataDeEnvio;
	}
	
	public String getConteudo() {
		return conteudo;
	}
	
	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}
	
	public int getTipo() {
		return tipo;
	}
	
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	
	public boolean isAtivo() {
		return ativo;
	}
	
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public String getCpfRemetente() {
		return cpfRemetente;
	}

	public void setCpfRemetente(String cpfRemetente) {
		this.cpfRemetente = cpfRemetente;
	}
	
	public String getCpfDestinatario() {
		return cpfDestinatario;
	}
	
	public void setCpfDestinatario(String cpfDestinatario) {
		this.cpfDestinatario = cpfDestinatario;
	}
	
}
