package br.com.yellowpages.modelo;

import java.util.Calendar;

public class Avaliacao {

	private Contrato contrato;
	private Usuario avaliador;
	private Usuario sujeito;
	private Calendar data;
	private String avaliacao;
	private String comentario;
	
	/**
	 * Construtor para instanciar objeto Avaliacao
	 * @param builder
	 */
	public Avaliacao() {
		
	}
	public Avaliacao(Contrato contrato, Usuario avaliador, Usuario sujeito, Calendar data, String avaliacao, String comentario) {
		this.contrato = contrato;
		this.avaliador = avaliador;
		this.sujeito = sujeito;
		this.data = data;
		this.avaliacao = avaliacao;
		this.comentario = comentario;
	}

	// Metodos gets e sets
	
	public Contrato getContrato() {
		return contrato;
	}
	
	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}
	
	public Usuario getAvaliador() {
		return avaliador;
	}
	
	public void setAvaliador(Usuario avaliador) {
		this.avaliador = avaliador;
	}
	
	public Usuario getSujeito() {
		return sujeito;
	}
	
	public void setSujeito(Usuario sujeito) {
		this.sujeito = sujeito;
	}
	
	public Calendar getData() {
		return data;
	}
	
	public void setData(Calendar data) {
		this.data = data;
	}
	
	public String getAvaliacao() {
		return avaliacao;
	}
	
	public void setAvaliacao(String avaliacao) {
		this.avaliacao = avaliacao;
	}
	
	public String getComentario() {
		return comentario;
	}
	
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
		
}
