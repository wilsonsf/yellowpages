package br.com.yellowpages.modelo;

import java.util.List;

public class Usuario {
        
    //Campos
    
    protected String cpf;
    protected String nome;
    protected String email;
    protected String telefone;
    protected List<String> categorias;
                
    public Usuario () {
    }
    
    public Usuario(String cpf, String nome, String email, String telefone, List<String> categorias) {
        this.cpf      = cpf;
        this.nome     = nome;
        this.email    = email;
        this.telefone = telefone;
        this.categorias = categorias;
    }
    
    // Metodos gets e sets
    
    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public List<String> getCategorias() {
        return categorias;
    }

    public void setCategorias(List<String> categorias) {
        this.categorias = categorias;
    }
        
}
