package br.com.yellowpages.modelo;

import java.util.Calendar;

public class Contrato {

	private int id;
	private Usuario cliente;
	private Usuario prestador;
	private Calendar dataInicio;
	private Calendar dataFinal;
	private String situacao;
	
	/**
	 * Construtor para instanciar objeto Contrato
	 * @param id
	 * @param cliente
	 * @param prestador
	 * @param dataInicio
	 * @param dataFinal
	 * @param situacao
	 */
	
//	public Contrato(ContratoBuilder builder) {
//		this.id = builder.id;
//		this.cliente = builder.cliente;
//		this.prestador = builder.prestador;
//		this.dataInicio = builder.dataInicio;
//		this.dataFinal = builder.dataFinal;
//		this.situacao = builder.situacao;
//	}
	public Contrato() {
	}
	public Contrato(int id, Usuario cliente, Usuario prestador, Calendar dataInicio, Calendar dataFinal, String situacao) {
		this.id = id;
		this.cliente = cliente;
		this.prestador = prestador;
		this.dataInicio = dataInicio;
		this.dataFinal = dataFinal;
		this.situacao = situacao;
	}
	
	// Metodos gets e sets
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public Usuario getCliente() {
		return cliente;
	}
	
	public void setCliente(Usuario cliente) {
		this.cliente = cliente;
	}
	
	public Usuario getPrestador() {
		return prestador;
	}
	
	public void setPrestador(Usuario prestador) {
		this.prestador = prestador;
	}
	
	public Calendar getDataInicio() {
		return dataInicio;
	}
	
	public void setDataInicio(Calendar dataInicio) {
		this.dataInicio = dataInicio;
	}
	
	public Calendar getDataFinal() {
		return dataFinal;
	}
	
	public void setDataFinal(Calendar dataFinal) {
		this.dataFinal = dataFinal;
	}
	
	public String getSituacao() {
		return situacao;
	}
	
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	
}
