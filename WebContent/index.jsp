<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<!-- <link rel="stylesheet" href="recursos/css/reset.css"> -->
		<link rel="stylesheet" href="recursos/css/estilos.css">
		<title>YellowPages</title>
	</head>
	<body>
		<header class="header container">
		  <!--[if lt IE 9]>
        <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <![endif]-->
		  <img src="http://placehold.it/140x140/" alt="YellowPages" />
	    <h1>Yellow Pages</h1>
		  <nav class="menu-opcoes">
			  <ul>
			    <li><a href="">Login</a></li>
			    <li><a href="">Cadastro</a></li>
			    <li><a href="">Contato</a></li>
			  </ul>
	    </nav>
	    <section class="busca">
		    <form action="">
	          <input type="text" placeholder="Digite um nome" id="nome">
	          <input type="submit" value="Pesquisar">
	      </form>
      </section>
	  </header>
	  <div class="container">
			<section class="menu-lateral" style="background-color: red">
			  <nav>
		      <ul>
		        <li>Categoria1</li>
		        <li>Categoria2</li>
		        <li>Categoria3</li>
		        <li>Categoria4</li>
		        <li>Categoria5</li>
		      </ul>
	      </nav>
			</section>
			<section class="conteudo">
			</section>
		</div>
		<footer class="footer">
		  <div class="container">
		    <p>(c) Copyrights. YellowPages 2015-2015</p>
		  </div>
	  </footer>
	</body>
</html>